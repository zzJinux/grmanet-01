#pragma once

#include <cstdio>

#include <GL/glew.h>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtc/epsilon.hpp>

#include "constants.h"
#include "props.h"
#include "objects_gl.h"
#include "custom_transforms.h"

extern GLint loc_ModelViewProjectionMatrix;
extern glm::mat4 ViewProjectionMatrix;

void Turret::activate(glm::vec2 const& center, GLfloat radius) {
	this->active = true;
	this->center = center;
	this->radius = radius;
}

void Turret::render() {
	int const &CAR_UPPER_HEIGHT = 10;
	int const &CAR_HEIGHT = 22;

	glm::mat4 modelMat, mvp, align;


	align = glm::rotate(glm::mat4(1), glm::half_pi<GLfloat>(), glm::vec3(0, 0, 1));
	align = glm::scale(align, glm::vec3(2, 2, 0));
	align = glm::translate(align, glm::vec3(0, -CAR_UPPER_HEIGHT, 0));

	modelMat = glm::rotate(glm::mat4(1), theta, glm::vec3(0, 0, 1));
	modelMat = glm::translate(modelMat, glm::vec3(radius - CAR_HEIGHT*2, 0, 0));
	modelMat = mozzi(modelMat, glm::vec2(1.75f, 1.75f), glm::vec2(1.0f, 1.0f), 1000, curMillis);
	modelMat *= align;
	mvp = ::ViewProjectionMatrix * modelMat;
	glUniformMatrix4fv(loc_ModelViewProjectionMatrix, 1, GL_FALSE, &mvp[0][0]);
	draw_car();

	modelMat = glm::rotate(glm::mat4(1), theta, glm::vec3(0, 0, 1));
	modelMat = glm::translate(modelMat, glm::vec3(radius - (CAR_HEIGHT - CAR_UPPER_HEIGHT)*2, 0, 0));
	modelMat *= align;
	mvp = ::ViewProjectionMatrix * modelMat;
	glUniformMatrix4fv(loc_ModelViewProjectionMatrix, 1, GL_FALSE, &mvp[0][0]);
	draw_car();
}

void Turret::update(int curMillis) {
	GLfloat const &PI16 = 16 * glm::two_pi<GLfloat>();
	this->curMillis = curMillis;

	theta = static_cast<GLfloat>(curMillis) * TURRET_ANGULAR_VELOCITY;
	if(theta > PI16) theta = fmod(theta, PI16);
}

void GunSmokeParticle::render() {
	renderSingle(-cF1);
	renderSingle(-cF2);
	renderSingle(cF1);
	renderSingle(cF2);
}

void GunSmokeParticle::renderSingle(GLfloat curveFactor, GLfloat offset) {
	int const &SHIRT_UPPER_HEIGHT = 10;

	glm::mat4 modelMat, mvp, align;

	align = glm::rotate(glm::mat4(1), glm::half_pi<GLfloat>(), glm::vec3(0, 0, 1));
	// align = glm::scale(align, glm::vec3(1, 1, 0));
	align = glm::translate(align, glm::vec3(0, -SHIRT_UPPER_HEIGHT, 0));
	
	GLfloat x = deltaMillis * GUNSMOKE_SPEED;
	GLfloat tangentAngle = glm::atan(2 * curveFactor * x);

	modelMat = glm::rotate(glm::mat4(1), theta, glm::vec3(0, 0, 1));
	modelMat = glm::translate(modelMat, glm::vec3(radius+16, 0, 0));
	modelMat = glm::translate(modelMat, glm::vec3(x, curveFactor*x*x + glm::sign(curveFactor)*offset, 0));
	modelMat = glm::rotate(modelMat, tangentAngle, glm::vec3(0, 0, 1));
	modelMat *= align;

	mvp = ::ViewProjectionMatrix * modelMat;
	glUniformMatrix4fv(loc_ModelViewProjectionMatrix, 1, GL_FALSE, &mvp[0][0]);
	draw_shirt();
}

void GunSmokeParticle::update(int curMillis) {
	if(!active) return;

	deltaMillis = curMillis - beginMillis;
	if(deltaMillis > GUNSMOKE_DURATION) {
		active = false;
	}
}

void GunSmokeParticle::activate(int beginMillis, glm::vec2 const&center, GLfloat radius, GLfloat theta, GLfloat cF1, GLfloat cF2) {
	this->beginMillis = beginMillis;
	this->deltaMillis = 0;
	this->center = center;
	this->radius = radius;
	this->theta = theta;
	this->cF1 = cF1;
	this->cF2 = cF2;

	this->active = true;
}

void Missile::render() {
	GLfloat const &PLANE_UPPER_HEIGHT = 25;
	GLfloat const &PLANE_HEIGHT = 50;
	glm::mat4 modelMat, mvp, align;

	align = glm::rotate(glm::mat4(1), glm::half_pi<GLfloat>(), glm::vec3(0, 0, 1));
	align = glm::scale(align, glm::vec3(.75f, .75f, 0));
	align = glm::translate(align, glm::vec3(0, -PLANE_UPPER_HEIGHT, 0));

	modelMat = glm::rotate(glm::mat4(1), theta, glm::vec3(0, 0, 1));
	modelMat = glm::translate(modelMat, glm::vec3(radius+32, 0, 0));
	modelMat = kumtul(modelMat, PLANE_HEIGHT, 2.25f, 250.0f, deltaMillis);
	modelMat *= align;

	mvp = ::ViewProjectionMatrix * modelMat;
	glUniformMatrix4fv(loc_ModelViewProjectionMatrix, 1, GL_FALSE, &mvp[0][0]);
	draw_airplane();
}

void Missile::update(int curMillis) {
	if(!active) return;

	deltaMillis = curMillis - beginMillis;
	if(deltaMillis > MISSILE_DURATION) {
		active = false;
	}
}

void Missile::activate(int beginMillis, glm::vec2 const&center, GLfloat radius, GLfloat theta) {
	this->beginMillis = beginMillis;
	this->deltaMillis = 0;
	this->center = center;
	this->radius = radius;
	this->theta = theta;

	this->active = true;
}

void MarchLine::render() {
	glm::mat4 modelMat, align, mvp;

	align = glm::rotate(glm::mat4(1), glm::half_pi<GLfloat>(), glm::vec3(0, 0, 1));
	align = glm::scale(align, glm::vec3(1.0f, 1.0f, 0));

	GLint num = glm::floor(glm::distance(P1, P2) / spacing) + 3;
	auto a = P2 - P1;
	for(int i = 0; i < num; ++i) {
		for(int j = 1; j <= 3; ++j) {
			modelMat = glm::translate(glm::mat4(1.0f), glm::vec3(P1, 0));
			modelMat *= glm::orientation(glm::normalize(glm::vec3(P2 - P1, 0)), glm::vec3(1, 0, 0));
			modelMat = glm::translate(modelMat, glm::vec3(spacing*i, crossSpacing*j, 0));
			modelMat = mozzi(modelMat, glm::vec2(3.0f), glm::vec2(1.75f), 200.0f, curMillis);
			modelMat *= align;

			mvp = ::ViewProjectionMatrix * modelMat;
			glUniformMatrix4fv(loc_ModelViewProjectionMatrix, 1, GL_FALSE, &mvp[0][0]);

			if(which == 0) draw_sword();
			else if(which == 1) draw_cake();
			else if(which == 2) draw_cocktail();
			else draw_shirt();
		}
	}
}

void MarchLine::update(int curMillis) {
	if(!active) return;

	this->curMillis = curMillis;
}

void MarchLine::activate(int curMillis, glm::vec2 const & P1, glm::vec2 const & P2, GLfloat crossSpacing, GLfloat spacing, GLuint which) {
	this->curMillis = curMillis;
	this->P1 = P1;
	this->P2 = P2;
	this->crossSpacing = crossSpacing;
	this->spacing = spacing;
	this->which = which;

	this->active = true;
}
