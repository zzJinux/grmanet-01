#include <stdio.h>
#include <stdlib.h>
#include <GL/glew.h>
#include <GL/freeglut.h>

#include "Shaders/LoadShaders.h"
GLuint h_ShaderProgram; // handle to shader program
GLint loc_ModelViewProjectionMatrix, loc_primitive_color; // indices of uniform variables

// include glm/*.hpp only if necessary
//#include <glm/glm.hpp> 
#include <glm/gtc/matrix_transform.hpp>
#include <vector>

#include "objects_gl.h"
#include "props.h"
#include "constants.h"
#include "custom_transforms.h"
#include "Simple2DTransformation.h"

int win_width = 0, win_height = 0; 
float centerx = 0.0f, centery = 0.0f, rotate_angle = 0.0f;

int initMillis, lastMillis, curMillis;
void initTimer() {
	initMillis = lastMillis = curMillis = glutGet(GLUT_ELAPSED_TIME);
}
void updateTime() {
	curMillis = glutGet(GLUT_ELAPSED_TIME);
}

struct SceneEnv {
	Turret turret;
	std::vector<GunSmokeParticle> particles;
	std::vector<Missile> missiles;
	MarchLine march1;
	MarchLine march2;
	MarchLine march3;
	MarchLine march4;
	
	void prepare() {
		turret.activate(/* default param */);
		particles.resize(MAX_GUNSMOKE_UNIT);
		missiles.resize(MAX_MISSILE_UNIT);

	}

	void updateIdle(int curMillis) {
		if(turret.active) turret.update(curMillis);

		for(auto &e : particles) {
			if(e.active) {
				e.update(curMillis);
			}
		}

		for(auto &e : missiles) {
			if(e.active) {
				e.update(curMillis);
			}
		}

		if(march1.active) march1.update(curMillis);
		if(march2.active) march2.update(curMillis);
		if(march3.active) march3.update(curMillis);
		if(march4.active) march4.update(curMillis);
	}

	void handleClick(int curMillis) {
		int i;

		for(i = 0; i < particles.size(); ++i) {
			if(!particles[i].active) break;
		}

		if(i == particles.size()) {
			int ii, max = 0, maxIdx;
			for(ii = 0; ii < particles.size(); ++ii) {
				if(max < particles[i].deltaMillis) {
					max = particles[i].deltaMillis;
					maxIdx = ii;
				}
			}
			i = ii;
		}

		particles[i].activate(curMillis, turret.center, turret.radius, turret.theta, .01, .02);

		for(i = 0; i < missiles.size(); ++i) {
			if(!missiles[i].active) break;
		}

		if(i == missiles.size()) {
			int ii, max = 0, maxIdx;
			for(ii = 0; ii < missiles.size(); ++ii) {
				if(max < missiles[i].deltaMillis) {
					max = missiles[i].deltaMillis;
					maxIdx = ii;
				}
			}
			i = ii;
		}

		missiles[i].activate(curMillis, turret.center, turret.radius, turret.theta);
	}

	void handleResize() {
		glm::vec2 top(0, win_height / 2), bottom(0, -win_height / 2);
		glm::vec2 left(-win_width / 2, 0), right(win_width / 2, 0);

		march1.activate(curMillis, left, top, 75.0f, 50.0f, 0);
		march2.activate(curMillis, top, right, 75.0f, 50.0f, 1);
		march3.activate(curMillis, right, bottom, 75.0f, 50.0f, 2);
		march4.activate(curMillis, bottom, left, 75.0f, 50.0f, 3);
	}

	void render() {
		if(turret.active) turret.render();

		for(auto &e: particles) {
			if(e.active) {
				e.render();
			}
		}

		for(auto &e: missiles) {
			if(e.active) {
				e.render();
			}
		}

		if(march1.active) march1.render();
		if(march2.active) march2.render();
		if(march3.active) march3.render();
		if(march4.active) march4.render();
	}

} GlobalScene;

glm::mat4 ViewMatrix, ProjectionMatrix, ViewProjectionMatrix;

void display(void) {
	glClear(GL_COLOR_BUFFER_BIT);

	glUniformMatrix4fv(loc_ModelViewProjectionMatrix, 1, GL_FALSE, &ViewProjectionMatrix[0][0]);
	draw_axes();

	::GlobalScene.render();
	glFlush();	
}

void keyboard(unsigned char key, int x, int y) {
	switch (key) {
	case 27: // ESC key
		glutLeaveMainLoop(); // Incur destuction callback for cleanups.
		break;
	case ' ':
		updateTime();
		::GlobalScene.handleClick(curMillis);
		glutPostRedisplay();
		break;
	}
}

void special(int key, int x, int y) {
}

int leftbuttonpressed = 0;
void mouse(int button, int state, int x, int y) {
	if(button == GLUT_LEFT_BUTTON) {
		if(state == GLUT_UP) {
			leftbuttonpressed = 0;

			updateTime();
			::GlobalScene.handleClick(curMillis);
			glutPostRedisplay();
		}
		else if(state == GLUT_DOWN) {
			leftbuttonpressed = 1;
		}
	}

}

void motion(int x, int y) {
} 
	
void reshape(int width, int height) {
	win_width = width, win_height = height;
	
  	glViewport(0, 0, win_width, win_height);
	ProjectionMatrix = glm::ortho(-win_width / 2.0, win_width / 2.0, 
		-win_height / 2.0, win_height / 2.0, -1000.0, 1000.0);
	ViewProjectionMatrix = ProjectionMatrix * ViewMatrix;

	GlobalScene.handleResize();

	update_axes();

	glutPostRedisplay();
}

void cleanup() {
	cleanup_objects();
}

void idleCb() {
	updateTime();
	int elapsed = curMillis - lastMillis;
	if(elapsed > DURATION_MS) {
		lastMillis = curMillis;
		::GlobalScene.updateIdle(curMillis);
		glutPostRedisplay();
	}
}

void register_callbacks(void) {
	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	glutSpecialFunc(special);
	glutMouseFunc(mouse);
	glutMotionFunc(motion);
	glutReshapeFunc(reshape);
	glutCloseFunc(cleanup);
	glutIdleFunc(idleCb);
}

void prepare_shader_program(void) {
	ShaderInfo shader_info[3] = {
		{ GL_VERTEX_SHADER, "Shaders/simple.vert" },
		{ GL_FRAGMENT_SHADER, "Shaders/simple.frag" },
		{ GL_NONE, NULL }
	};

	h_ShaderProgram = LoadShaders(shader_info);
	glUseProgram(h_ShaderProgram);

	loc_ModelViewProjectionMatrix = glGetUniformLocation(h_ShaderProgram, "u_ModelViewProjectionMatrix");
	loc_primitive_color = glGetUniformLocation(h_ShaderProgram, "u_primitive_color");
}

void initialize_OpenGL(void) {
	glEnable(GL_MULTISAMPLE);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	glClearColor(.78, .78, .78, 1.0f);
	ViewMatrix = glm::mat4(1.0f);
}

void prepare_scene(void) {
	prepare_objects();
	::GlobalScene.prepare();
}

void initialize_renderer(void) {
	register_callbacks();
	prepare_shader_program(); 
	initialize_OpenGL();
	prepare_scene();
}

void initialize_glew(void) {
	GLenum error;

	glewExperimental = GL_TRUE;

    error = glewInit();
	if (error != GLEW_OK) { 
		fprintf(stderr, "Error: %s\n", glewGetErrorString(error));
		exit(-1);
	}
	fprintf(stdout, "*********************************************************\n");
	fprintf(stdout, " - GLEW version supported: %s\n", glewGetString(GLEW_VERSION));
	fprintf(stdout, " - OpenGL renderer: %s\n", glGetString(GL_RENDERER));
	fprintf(stdout, " - OpenGL version supported: %s\n", glGetString(GL_VERSION));
	fprintf(stdout, "*********************************************************\n\n");
}

void greetings(char *program_name, char messages[][256], int n_message_lines) {
	fprintf(stdout, "**************************************************************\n\n");
	fprintf(stdout, "  PROGRAM NAME: %s\n\n", program_name);
	fprintf(stdout, "    This program was coded for CSE4170 students\n");
	fprintf(stdout, "      of Dept. of Comp. Sci. & Eng., Sogang University.\n\n");

	for (int i = 0; i < n_message_lines; i++)
		fprintf(stdout, "%s\n", messages[i]);
	fprintf(stdout, "\n**************************************************************\n\n");
}

#define N_MESSAGE_LINES 2
int main(int argc, char *argv[]) {
	char program_name[64] = "Sogang CSE4170 1803HW1, 20131604";
	char messages[N_MESSAGE_LINES][256] = {
		"    - Keys used: 'ESC', four arrows",
		"    - Mouse used: L-click and move"
	};

	glutInit(&argc, argv);
 	glutInitDisplayMode(GLUT_RGBA | GLUT_MULTISAMPLE);
	glutInitWindowSize (1200*0.95, 800*0.95);
	glutInitContextVersion(4, 0);
	glutInitContextProfile(GLUT_CORE_PROFILE);
	glutCreateWindow(program_name);

	greetings(program_name, messages, N_MESSAGE_LINES);
	initialize_glew();
	initialize_renderer();

	initTimer();

	glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_GLUTMAINLOOP_RETURNS);
	glutMainLoop();

	return 0;
}
