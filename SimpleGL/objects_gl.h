#pragma once

#include "glm/gtc/matrix_transform.hpp"

void prepare_axes(void);
void update_axes(void);
void draw_axes(void);

void prepare_airplane(void);
void draw_airplane(void);

void prepare_shirt(void);
void draw_shirt(void);

void prepare_house(void);
void draw_house(void);

void prepare_car(void);
void draw_car(void);

void prepare_cocktail(void);
void draw_cocktail(void);

void prepare_car2(void);
void draw_car2(void);

void prepare_hat(void);
void draw_hat(void);

void prepare_cake(void);
void draw_cake(void);

void prepare_sword(void);
void draw_sword(void);

void drawObjectsExamples(glm::mat4 const &ViewProjectionMatrix);

void prepare_objects(void);

void cleanup_objects(void);