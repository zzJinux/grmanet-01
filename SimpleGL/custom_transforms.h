#pragma once

#include <GL/glew.h>
#include <glm/gtc/matrix_transform.hpp>

glm::mat4 rotateAround(glm::mat4 const &lh, glm::vec3 const &center, GLfloat const &rad);

glm::mat4 mozzi(glm::mat4 const &lh, glm::vec2 const &maxScale, glm::vec2 const &minScale, GLfloat period, GLfloat t);

glm::mat4 kumtul(glm::mat4 const &lh, GLfloat unitLength, GLfloat scaleFactor, GLfloat period, GLfloat t);