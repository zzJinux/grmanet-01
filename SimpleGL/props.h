#pragma once

#include <GL/glew.h>
#include <glm/vec2.hpp>

struct Turret {
	bool active = false;

	glm::vec2 center;
	GLfloat radius;
	GLfloat theta;
	int curMillis;

	void render();
	void update(int curMillis);

	void activate(glm::vec2 const& center = glm::vec2(0, 0), GLfloat radius = 44.0f);
};

struct GunSmokeParticle {
	bool active = false;

	int beginMillis;
	int deltaMillis;
	glm::vec2 center;
	GLfloat radius;
	GLfloat theta;
	GLfloat cF1, cF2;

	void render();
	void renderSingle(GLfloat curveFactor, GLfloat offset = 20.0f);
	void update(int curMillis);

	void activate(int beginMillis, glm::vec2 const &center, GLfloat radius, GLfloat theta, GLfloat cF1, GLfloat cF2);
};

struct Missile {
	bool active = false;

	int beginMillis;
	int deltaMillis;
	glm::vec2 center;
	GLfloat radius;
	GLfloat theta;

	void render();
	void update(int curMillis);

	void activate(int beginMillis, glm::vec2 const &center, GLfloat radius, GLfloat theta);
};


struct MarchLine {
	bool active = false;

	int curMillis;
	glm::vec2 P1, P2;
	GLfloat crossSpacing, spacing;
	GLuint which;

	void render();
	void update(int curMillis);

	void activate(int curMillis, glm::vec2 const &P1, glm::vec2 const &P2, GLfloat crossSpacing, GLfloat spacing, GLuint which);
};