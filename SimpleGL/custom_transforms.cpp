#pragma once

#include <GL/glew.h>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/constants.hpp>

glm::mat4 rotateAround(glm::mat4 const &lh, glm::vec3 const &center, GLfloat const &rad) {
	glm::mat4 Matrix;
	Matrix = glm::translate(lh, center);
	Matrix = glm::rotate(Matrix, rad, glm::vec3(0.0f, 0.0f, 1.0f));
	return glm::translate(Matrix, -center);
}

glm::mat4 mozzi(glm::mat4 const &lh, glm::vec2 const &maxScale, glm::vec2 const &minScale, GLfloat period, GLfloat t) {
	auto const &PI_2 = glm::two_pi<GLfloat>();
	return glm::scale(lh, glm::vec3(
		(maxScale + minScale) * .5f +
		(maxScale - minScale) * .5f * glm::sin(PI_2 / period * glm::vec2(t - period / 4., t + period / 4.)),
		1.
	));
}

glm::mat4 kumtul(glm::mat4 const &lh, GLfloat unitLength, GLfloat scaleFactor, GLfloat period, GLfloat t) {
	auto s = (scaleFactor - 1) / 2 * glm::sin(glm::two_pi<GLfloat>() / period * (t - period / 4.)) + (scaleFactor + 1) / 2;
	auto L = unitLength * (scaleFactor - 1);
	auto i = static_cast<int>(glm::floor(2 * t / period));
	GLfloat d;
	if(i % 2) {
		d = L * (i / 2) + unitLength * (scaleFactor - s);
	}
	else {
		d = L * (i / 2);
	}

	glm::mat4 Matrix;
	Matrix = glm::translate(lh, glm::vec3(d, 0, 0));
	return glm::scale(Matrix, glm::vec3(s, 1, 1));
}